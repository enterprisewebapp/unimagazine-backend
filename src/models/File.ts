import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";

const Schema = mongoose.Schema;

let fileSchema = new Schema({
    fileId: { type: Number, required: true },
    path: { type: String, required: true},
    fileType: {type: String, required: true},
    createdDate: { type: Date, default: Date.now, required: true },
    updatedDate: { type: Date, default: Date.now, required: true }
});

autoIncrement.initialize(mongoose.connection);

fileSchema.plugin(autoIncrement.plugin, {
    model: "File",
    field: "fileId",
    incrementBy: 1,
    startAt: 1
});
fileSchema.index({ fileId: 1 }, { unique: true });

export const File = mongoose.model("File", fileSchema, 'files')