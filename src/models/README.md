# Models | Information
## by Nguyen Anh Hoang
### Last updated Feb 28th 2019

* User
* Thread
  * Status: 0: Pending, 1: Need to edit, 2: Approved, 3: Cancelled.
* ThreadFile
* ThreadImage
* ThreadComment
* Faculty
* YearConfig