import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";

const Schema = mongoose.Schema;

let yearConfigSchema = new Schema({
  configId: { type: Number, required: true },
  academicYear: { type: Number, required: true },
  termContent: { type: String, required: true },
  closureDate1: { type: Date, default: Date.now, required: true },
  closureDate2: { type: Date, default: Date.now, required: true },
  currentFlag: {
    type: Number,
    enum: [0, 1],
    default: 0
  },
  createdDate: { type: Date, default: Date.now, required: true },
  updatedDate: { type: Date, default: Date.now, required: true }
});

autoIncrement.initialize(mongoose.connection);
yearConfigSchema.plugin(autoIncrement.plugin, {
  model: "YearConfig",
  field: "configId",
  incrementBy: 1
});
yearConfigSchema.index({ configId: 1 }, { unique: true });

export const YearConfig = mongoose.model("YearConfig", yearConfigSchema, "yearConfigs");
