import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";

const Schema = mongoose.Schema;

let threadSchema = new Schema({
  threadId: { type: Number, required: true },
  threadTitle: { type: String, required: true },
  description: { type: String, required: true },
  faculty: { type: String, required: true },
  createdBy: { type: Schema.Types.ObjectId, required: true, ref: "User" },
  takenBy: { type: Schema.Types.ObjectId, default: null, ref: "User" },
  status: {
    type: String,
    enum: [0, 1, 2, 3],
    default: 0
  },
  threadComments: [{ type: Schema.Types.ObjectId, ref: "ThreadComment" }],
  threadFiles: [{ type: Schema.Types.ObjectId, ref: "File" }],
  createdDate: { type: Date, default: Date.now, required: true },
  updatedDate: { type: Date, default: Date.now, required: true }
});

autoIncrement.initialize(mongoose.connection);
threadSchema.plugin(autoIncrement.plugin, {
  model: "Thread",
  field: "threadId",
  incrementBy: 1
});
threadSchema.index({ threadId: 1 }, { unique: true });

export const Thread = mongoose.model("Thread", threadSchema, "threads");
