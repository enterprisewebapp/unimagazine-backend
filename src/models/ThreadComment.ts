import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";

const Schema = mongoose.Schema;

let threadCommentSchema = new Schema({
  commentId: { type: Number, required: true },
  content: { type: String, required: true},
  createdBy: { type: Schema.Types.ObjectId, required: true, ref: 'User'},
  createdDate: { type: Date, default: Date.now, required: true },
  updatedDate: { type: Date, default: Date.now, required: true }
});

autoIncrement.initialize(mongoose.connection);
threadCommentSchema.plugin(autoIncrement.plugin, {
  model: "ThreadComment",
  field: "commentId",
  incrementBy: 1
});
threadCommentSchema.index({ commentId: 1 }, { unique: true });

export const ThreadComment = mongoose.model("ThreadComment", threadCommentSchema, "threadComments");
