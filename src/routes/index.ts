import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import {
	ROOT,
	USER_LOGIN,
	USER_REGISTER,
	USER_UPDATE_INFO,
	USER_UPDATE_PASSWORD,
	USER_LIST,
	USER_LIST_BY_ROLE,
	ADM_SET_ROLE,
	ADM_SET_FACULTY,
	YEARTCONFIG_LIST,
	YEARCONFIG_CREATE,
	YEARCONFIG_UPDATE,
	YEARCONFIG_UPDATE_STATUS,
	THREAD_LIST,
	THREAD_CREATE,
	THREAD_LIST_FACULTY,
	THREAD_COMMENT,
	THREAD_UPDATE,
	THREAD_UPDATE_STATUS,
	THREAD_LIST_STATUS,
	USER_LIST_BY_FACULTY
} from '../core/constant';
import { UserController } from '../controllers/UserController';
import { YearConfigController } from '../controllers/YearConfigController';
import { ThreadController } from '../controllers/ThreadController';

const storage = multer.diskStorage({
	destination(req, file, cb) {
		cb(null, 'public/uploads');
	},
	filename(req, file, cb) {
		cb(null, Date.now() + '-' + file.originalname.split(' ').join('_'));
	}
});
const upload = multer({ storage });

export class Routes {
	public userController: UserController = new UserController();
	public yrCfgController: YearConfigController = new YearConfigController();
	public threadController: ThreadController = new ThreadController();

	public routes(app: any): void {
		// ROOT
		app.route(ROOT).get((req: Request, res: Response) => {
			res.status(200).send({
				message: 'UniMagazine Backend'
			});
		});

		// USER
		app.route(USER_LOGIN).post(this.userController.userLogin);
		app.route(USER_REGISTER).post(this.userController.registerUser);
		app.route(USER_UPDATE_INFO).post(this.userController.updateUserInfo);
		app.route(USER_UPDATE_PASSWORD).post(this.userController.updatePassword);
		app.route(USER_LIST).get(this.userController.listUsr);
		app.route(USER_LIST_BY_ROLE).get(this.userController.listUsrByRole);
		app.route(USER_LIST_BY_FACULTY).get(this.userController.listUsrByRole);

		// ADM
		app.route(ADM_SET_FACULTY).post(this.userController.updateUserFaculty);
		app.route(ADM_SET_ROLE).post(this.userController.updateRole);

		//YEARCONFIG
		app.route(YEARTCONFIG_LIST).get(this.yrCfgController.listsYrCfg);
		app.route(YEARCONFIG_CREATE).post(this.yrCfgController.addNewYrCfg);
		app.route(YEARCONFIG_UPDATE).post(this.yrCfgController.updateYrCfg);
		app.route(YEARCONFIG_UPDATE_STATUS).post(this.yrCfgController.setCurrentYrCfg);

		//THREAD
		app.route(THREAD_LIST).get(this.threadController.listThread);
		app.route(THREAD_CREATE).post(upload.any(), this.threadController.createThread);
		app.route(THREAD_LIST_FACULTY).get(this.threadController.listThreadByFaculty);
		app.route(THREAD_LIST_STATUS).get(this.threadController.listThreadByStatus);
		app.route(THREAD_COMMENT).post(this.threadController.addComment);
		app.route(THREAD_UPDATE).post(upload.any(), this.threadController.updateThread);
		app.route(THREAD_UPDATE_STATUS).post(this.threadController.updateThreadStatus);
		app.route('/api/v1/test/sendmail').post(this.threadController.testSendMail);
	}
}
