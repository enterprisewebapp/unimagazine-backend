import path from 'path';
import nodemailer, { Transporter } from 'nodemailer';
import ejs from 'ejs';
import { MAIL_USERNAME, MAIL_PASS } from '../core/constant';

const transporter: Transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'hoanthan317@gmail.com',
		pass: 'hoan31798'
	}
});

const sendMail = (option: any) => {
	option.text = `Hi,\nPlease comment student contributions.`;
	option.html = `<p>Hi,<br/>Please comment student contributions.</p>`;
	transporter.sendMail(option, (error, info) => {
		if (error) return console.error(error);
		console.log(info);
	});
};

export { transporter, sendMail };
