import dotenv from 'dotenv';

dotenv.load();

// ENV
export const JWT_CHARS = process.env.JWT_CHARS;
export const APP_PORT = process.env.PORT || 3000;
export const MONGODB_URI = process.env.MONGODB_URI || '';
export const MAIL_HOST = process.env.MAIL_HOST || 'smtp.gmail.com';
export const MAIL_PORT:number = parseInt(process.env.MAIL_POST || "465");
export const MAIL_USERNAME = process.env.MAIL_USERNAME;
export const MAIL_PASS = process.env.MAIL_PASS;
export const MAIL_FROM = process.env.MAIL_FROM;

// ROUTES
// ----ROOT
export const ROOT = '/api/v1';
// ----USER
export const USER = ROOT + '/user';
export const USER_LIST = USER + '/list';
export const USER_LIST_BY_ROLE = USER_LIST + '/role';
export const USER_LIST_BY_FACULTY = USER_LIST + '/faculty';
export const USER_LOGIN = USER + '/login';
export const USER_REGISTER = USER + '/register';
export const USER_UPDATE_INFO = USER + '/update-info';
export const USER_UPDATE_PASSWORD = USER + '/update-password';
// ----ADM
export const ADM = ROOT + '/adm';
export const ADM_SET_ROLE = ADM + '/set-role';
export const ADM_SET_FACULTY = ADM + '/set-faculty';
// ----THREAD
export const THREAD = ROOT + '/thread';
export const THREAD_LIST = THREAD + '/list';
export const THREAD_CREATE = THREAD + '/create';
export const THREAD_UPDATE = THREAD + '/update';
export const THREAD_UPDATE_STATUS = THREAD_UPDATE + '/status';
export const THREAD_DELETE = THREAD + '/delete';
export const THREAD_DETAILS = THREAD + '/details';
export const THREAD_LIST_FACULTY = THREAD_LIST + '/faculty';
export const THREAD_LIST_STATUS = THREAD_LIST + '/status';
export const THREAD_COMMENT = THREAD + '/comment';
// ----YEARCONFIG
export const YEARCONFIG = ROOT + '/year-config';
export const YEARTCONFIG_LIST = YEARCONFIG + '/list';
export const YEARCONFIG_CREATE = YEARCONFIG + '/create';
export const YEARCONFIG_UPDATE = YEARCONFIG + '/update';
export const YEARCONFIG_UPDATE_STATUS = YEARCONFIG_UPDATE + '/status';
