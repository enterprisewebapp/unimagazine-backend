import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { YearConfig } from '../models/YearConfig';
import { JWT_CHARS } from '../core/constant';

const jwtChars: any = JWT_CHARS;

export class YearConfigController {
	public addNewYrCfg(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { role } = data;
				if (role === 'admin') {
					const { academicYear, termContent, closureDate1, closureDate2, currentFlag } = body;
					let yrCfg = new YearConfig({
						academicYear,
						termContent,
						closureDate1,
						closureDate2,
						currentFlag
					});
					yrCfg.save(function(err: any, result: any) {
						if (err) return res.status(500).json(err);
						return res.status(200).json({ message: 'saved' } || {});
					});
				} else {
					return res.status(200).json({ message: 'invalid_role' } || {});
				}
			});
		}
	}

	public updateYrCfg(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { role } = data;
				if (role === 'admin') {
					const { configId, academicYear, termContent, closureDate1, closureDate2 } = body;
					YearConfig.findOneAndUpdate(
						{ configId },
						{
							$set: {
								academicYear,
								termContent,
								closureDate1,
								closureDate2
							}
						},
						{ new: true },
						(err, user) => {
							if (err) return res.status(500).json(err);
							return res.status(200).json({ message: 'updated_yr_cfg' } || {});
						}
					);
				} else {
					return res.status(200).json({ message: 'invalid_role' } || {});
				}
			});
		}
	}

	public setCurrentYrCfg(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { role } = data;
				if (role === 'admin') {
					const { configId } = body;
					const unsetCurrentFlg = await YearConfig.updateMany({}, { currentFlag: 0 }).exec();
					YearConfig.findOneAndUpdate(
						{ configId },
						{
							$set: {
								currentFlag: 1
							}
						},
						{ new: true },
						(err, user) => {
							if (err) return res.status(500).json(err);
							return res.status(200).json({ message: 'updated_crnt_yr_cfg' } || {});
						}
					);
				} else {
					return res.status(200).json({ message: 'invalid_role' } || {});
				}
			});
		}
	}

	public async listsYrCfg(req: Request, res: Response) {
		const { query } = req;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			const { pageIndex, pageSize } = query;
			const offset = pageIndex * pageSize;
			const limit = parseInt(pageSize, 10);
			const yrCfgLst = await YearConfig.find().skip(offset).limit(limit).exec();
			YearConfig.count({}, (error, count) => {
				if (error) {
					return res.status(500).json({ message: error.message });
				}
				return res.status(200).json({ yrCfgLst, total: count });
			});
		}
	}
}
