import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../models/User';
import { Thread } from '../models/Thread';
import { ThreadComment } from '../models/ThreadComment';
import { File } from '../models/File';
import { JWT_CHARS } from '../core/constant';
import { MAIL_FROM } from '../core/constant';
import { sendMail } from '../config/email';
import { YearConfig } from '../models/YearConfig';

const jwtChars: any = JWT_CHARS;

export class ThreadController {
	public createThread(req: any, res: any) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(500).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role, _id, userId, faculty } = data;
				if (role === 'marketing_manager' || role === 'marketing_coordinator' || role === 'guest') {
					return res.status(403).json({ message: 'forbidden' });
				}
				const { files } = req;
				let images: any = [];
				let wordFile: any = [];
				files.forEach((element: any) => {
					if (element.fieldname === 'images') {
						images.push(element);
					} else if (element.fieldname === 'wordFile') {
						wordFile.push(element);
					}
				});
				const { threadTitle, description } = body;
				// tslint:disable-next-line
				let threadFiles: any = [];
				// tslint:disable-next-line
				for (let i = 0; i < images.length; i++) {
					// tslint:disable-next-line
					let file = new File({
						path: images[i].path,
						fileType: 'img'
					});
					const res = await file.save();
					threadFiles.push(res._id);
				}
				for (let i = 0; i < wordFile.length; i++) {
					// tslint:disable-next-line
					let file = new File({
						path: wordFile[i].path,
						fileType: 'word'
					});
					const res = await file.save();
					threadFiles.push(res._id);
				}
				// tslint:disable-next-line
				let thread = new Thread({
					threadTitle,
					description,
					faculty,
					threadFiles,
					createdBy: _id
				});
				const threadRes = await thread.save();
				const currentUsr: any = await User.findOne({ userId }).exec();
				let { threads } = currentUsr;
				threads.push(threadRes._id);
				User.findOneAndUpdate(
					{ userId },
					{
						$set: {
							threads
						}
					},
					{ new: true },
					(err, user) => {
						if (err) return res.status(500).json(err);
						return res.status(200).json({ message: 'saved' } || {});
					}
				);
				User.find(
					{
						faculty,
						role: 'marketing_coordinator'
					},
					(error, docs) => {
						if (err) return console.error('Get coordinator error', error);
						var coordinator = docs[0];
						if (coordinator)
							sendMail({
								from: MAIL_FROM,
								to: `<${(coordinator as any).email}>`,
								subject: 'New contribution uploaded!'
							});
					}
				);
			});
		}
	}

	public updateThread(req: any, res: any) {
		const { body } = req;
		console.log(req);
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(500).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role } = data;
				if (role === 'marketing_manager' || role === 'marketing_coordinator' || role === 'guest') {
					return res.status(403).json({ message: 'forbidden' });
				}
				const { files } = req;
				let images: any = [];
				let wordFile: any = [];
				files.forEach((element: any) => {
					if (element.fieldname === 'images') {
						images.push(element);
					} else if (element.fieldname === 'wordFile') {
						wordFile.push(element);
					}
				});
				const { threadId } = body;
				// tslint:disable-next-line
				let threadFiles: any = [];
				// tslint:disable-next-line
				for (let i = 0; i < images.length; i++) {
					// tslint:disable-next-line
					let file = new File({
						path: images[i].path,
						fileType: 'img'
					});
					const res = await file.save();
					threadFiles.push(res._id);
				}
				for (let i = 0; i < wordFile.length; i++) {
					// tslint:disable-next-line
					let file = new File({
						path: wordFile[i].path,
						fileType: 'word'
					});
					const res = await file.save();
					threadFiles.push(res._id);
				}
				Thread.findOneAndUpdate(
					{ threadId },
					{
						$set: {
							threadFiles,
							status: 0
						}
					},
					{ new: true },
					(err, user) => {
						if (err) return res.status(500).json(err);
						return res.status(200).json({ message: 'updated' } || {});
					}
				);
			});
		}
	}

	public updateThreadStatus(req: Request, res: Response) {
		const { body } = req;
		console.log(req);
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(500).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role, _id } = data;
				if (role === 'student' || role === 'guest') {
					return res.status(403).json({ message: 'forbidden' });
				}
				const { threadId, threadStatus } = body;
				Thread.findOneAndUpdate(
					{ threadId },
					{
						$set: {
							status: threadStatus,
							takenBy: _id
						}
					},
					{ new: true },
					(err, user) => {
						if (err) return res.status(500).json(err);
						return res.status(200).json({ message: 'updated' } || {});
					}
				);
			});
		}
	}

	public async listThread(req: Request, res: Response) {
		const { query } = req;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			const { pageIndex, pageSize } = query;
			const offset = pageIndex * pageSize;
			const limit = parseInt(pageSize, 10);
			const threadLst = await Thread.find({})
				.skip(offset)
				.limit(limit)
				.populate({
					path: 'createdBy'
				})
				.populate({
					path: 'takenBy'
				})
				.populate({
					path: 'threadFiles'
				})
				.populate({
					path: 'threadComments',
					populate: {
						path: 'createdBy'
					}
				})
				.exec();
			Thread.count({}, (error, count) => {
				if (error) {
					return res.status(500).json({ message: error.message });
				}
				return res.status(200).json({ threadLst, total: count });
			});
		}
	}

	public async addComment(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(500).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role, _id } = data;
				if (role === 'marketing_manager' || role === 'guest') {
					return res.status(403).json({ message: 'forbidden' });
				} else {
					console.log(body);
					const { threadId, content } = body;
					let threadComment = new ThreadComment({
						content,
						createdBy: _id
					});
					const commentRes = await threadComment.save();
					console.log(commentRes);
					const currentThread: any = await Thread.findOne({ threadId }).exec();
					let { threadComments } = currentThread;
					threadComments.push(commentRes._id);
					Thread.findOneAndUpdate(
						{ threadId },
						{
							$set: {
								threadComments
							}
						},
						{ new: true },
						(err, user) => {
							if (err) return res.status(500).json(err);
							return res.status(200).json({ message: 'saved' } || {});
						}
					);
				}
			});
		}
	}

	public async listThreadByFaculty(req: Request, res: Response) {
		const { query } = req;
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role } = data;
				if (role === 'marketing_manager') {
					return res.status(403).json({ message: 'forbidden' });
				}
				const { pageIndex, pageSize, faculty } = query;
				const offset = pageIndex * pageSize;
				const limit = parseInt(pageSize, 10);
				var threadLst = await Thread.find({ faculty })
					.skip(offset)
					.limit(limit)
					.populate({
						path: 'createdBy'
					})
					.populate({
						path: 'takenBy'
					})
					.populate({
						path: 'threadFiles'
					})
					.populate({
						path: 'threadComments'
					})
					.exec();
				Thread.count({ faculty }, (error, count) => {
					if (error) {
						return res.status(500).json({ message: error.message });
					}
					if (role == 'guest') threadLst = threadLst.filter((thread) => thread.get('status') == 3);
					return res.status(200).json({ threadLst, total: count });
				});
			});
		}
	}

	public async listThreadByStatus(req: Request, res: Response) {
		const { query } = req;
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, async (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { role } = data;
				if (role === 'student' || role === 'guest') {
					return res.status(403).json({ message: 'forbidden' });
				}
				const { pageIndex, pageSize, threadStatus } = query;
				const offset = pageIndex * pageSize;
				const limit = parseInt(pageSize, 10);
				const threadLst = await Thread.find({ status: threadStatus })
					.skip(offset)
					.limit(limit)
					.populate({
						path: 'createdBy'
					})
					.populate({
						path: 'takenBy'
					})
					.populate({
						path: 'threadFiles'
					})
					.populate({
						path: 'threadComments'
					})
					.exec();
				Thread.count({ status: threadStatus }, (error, count) => {
					if (error) {
						return res.status(500).json({ message: error.message });
					}
					return res.status(200).json({ threadLst, total: count });
				});
			});
		}
	}

	public testSendMail(req: Request, res: Response) {
		const body = req.body;
		sendMail({
			from: MAIL_FROM,
			to: `User <${body.email}>`,
			subject: 'Test'
		});
		return res.json({ message: 'Success!' });
	}
}
