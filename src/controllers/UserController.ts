import { Request, Response } from 'express';
import bcrypt from 'bcrypt-nodejs';
import jwt from 'jsonwebtoken';
import { User } from '../models/User';
import { JWT_CHARS } from '../core/constant';

const jwtChars: any = JWT_CHARS;

export class UserController {
	public registerUser(req: Request, res: Response) {
		const { body } = req;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			User.find({ email: body.email }).exec((err, result) => {
				if (err) return res.status(500).json({ message: 'Something went wrong. Please try again later!' });
				if (result.length)
					return res
						.status(400)
						.json({ message: `User with email ${body.email} already existed. Please choose another one!` });
				const { password } = body;
				const salt = bcrypt.genSaltSync(10);
				const hashedPass = bcrypt.hashSync(password, salt);
				let user = new User({
					fullname: body.fullname,
					email: body.email,
					address: body.address,
					password: hashedPass,
					salt
				});
				user.save(function(err: any, result: any) {
					if (err) return res.status(500).json(err);
					return res.status(200).json({ message: 'saved' } || {});
				});
			});
		}
	}

	public userLogin(req: Request, res: Response) {
		const { body } = req;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			const { email } = body;
			const { psw } = body;
			User.findOne({ email }, (err: any, user: any) => {
				if (err) res.status(500).json({ message: err.message });
				if (user === null) {
					return res.status(200).json({ message: 'invalid_username' });
				}
				const { password } = user;
				if (bcrypt.compareSync(psw, password)) {
					const { _id, userId, email, role, faculty } = user;
					const data = {
						_id,
						userId,
						email,
						role,
						faculty
					};
					const token = jwt.sign({ data }, jwtChars, { expiresIn: '1h' });
					return res.status(200).json({ token, user });
				} else {
					return res.status(200).json({ message: 'invalid_password' });
				}
			});
		}
	}

	public updateUserInfo(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { userId } = data;
				User.findOneAndUpdate(
					{ userId },
					{
						$set: {
							fullname: body.fullname,
							email: body.email,
							address: body.address
						}
					},
					{ new: true },
					(err, user) => {
						if (err) return res.status(500).json(err);
						return res.status(200).json({ message: 'updated_user_info' } || {});
					}
				);
			});
		}
	}

	public updateRole(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { role } = data;
				if (role === 'admin') {
					const { usrId, usrRole } = body;
					User.findOneAndUpdate(
						{ userId: usrId },
						{
							$set: {
								role: usrRole
							}
						},
						{ new: true },
						(err, user) => {
							if (err) return res.status(500).json(err);
							return res.status(200).json({ message: 'updated_user_role' } || {});
						}
					);
				} else {
					return res.status(200).json({ message: 'invalid_role' });
				}
			});
		}
	}

	public updateUserFaculty(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) return res.status(403).json({ message: 'forbidden' });
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) return res.status(500).json(err);
				const { data } = decoded;
				const { role } = data;
				if (role === 'admin') {
					const { usrId, usrFaculty } = body;
					User.findOneAndUpdate(
						{ userId: usrId },
						{
							$set: {
								faculty: usrFaculty
							}
						},
						{ new: true },
						(err, user) => {
							if (err) return res.status(500).json(err);
							return res.status(200).json({ message: 'updated_user_faculty' } || {});
						}
					);
				} else {
					return res.status(200).json({ message: 'invalid_role' });
				}
			});
		}
	}

	public updatePassword(req: Request, res: Response) {
		const { body } = req;
		const { authorization } = req.headers;
		if (!authorization) {
			return res.status(403).json({ message: 'forbidden' });
		}
		const token: any = authorization;
		if (body.constructor === Object && Object.keys(body).length === 0) {
			return res.status(500).json({ message: 'req_body_check_failed' });
		} else {
			jwt.verify(token, jwtChars, (err: any, decoded: any) => {
				if (err) {
					return res.status(500).json(err);
				}
				const { data } = decoded;
				const { userId } = data;
				const { oldpsw, psw } = body;
				User.findOne({ userId }, (error: any, user: any) => {
					if (error) {
						return res.status(500).json({ message: error.message });
					}
					const { password } = user;
					if (!bcrypt.compareSync(oldpsw, password)) {
						return res.status(200).json({ message: 'incorrect_password' });
					}
					const salt = bcrypt.genSaltSync(10);
					const hashedPass = bcrypt.hashSync(psw, salt);
					User.findOneAndUpdate(
						{ userId },
						{
							$set: {
								password: hashedPass,
								salt
							}
						},
						{ new: true },
						(e: any, result: any) => {
							if (e) {
								return res.status(500).json(e);
							}
							return res.status(200).json({ message: 'updated_password' } || {});
						}
					);
				});
			});
		}
	}

	public async listUsr(req: Request, res: Response) {
		const { query } = req;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_query_check_failed' });
		} else {
			const { pageIndex, pageSize } = query;
			const offset = pageIndex * pageSize;
			const limit = parseInt(pageSize, 10);
			const usrLst = await User.find({}).skip(offset).limit(limit).exec();
			User.count({}, (error, count) => {
				if (error) {
					return res.status(500).json({ message: error.message });
				}
				return res.status(200).json({ usrLst, total: count });
			});
		}
	}

	public async listUsrByRole(req: Request, res: Response) {
		const { query } = req;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			const { pageIndex, pageSize, role } = query;
			const offset = pageIndex * pageSize;
			const limit = parseInt(pageSize, 10);
			const usrLst = await User.find({
				role
			})
				.skip(offset)
				.limit(limit)
				.exec();
			User.count({ role }, (error, count) => {
				if (error) {
					return res.status(500).json({ message: error.message });
				}
				return res.status(200).json({ usrLst, total: count });
			});
		}
	}
	public async listUsrByFaculty(req: Request, res: Response) {
		const { query } = req;
		if (query.constructor === Object && Object.keys(query).length === 0) {
			return res.status(200).json({ message: 'req_body_check_failed' });
		} else {
			const { pageIndex, pageSize, faculty } = query;
			const offset = pageIndex * pageSize;
			const limit = parseInt(pageSize, 10);
			const usrLst = await User.find({
				faculty
			})
				.skip(offset)
				.limit(limit)
				.exec();
			User.count({ faculty }, (error, count) => {
				if (error) {
					return res.status(500).json({ message: error.message });
				}
				return res.status(200).json({ usrLst, total: count });
			});
		}
	}
}
